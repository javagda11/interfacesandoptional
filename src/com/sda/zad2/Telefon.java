package com.sda.zad2;

import java.util.Random;

public class Telefon implements IDzwoni {
    private String nrTel;
    private int lacznyCzasRozmow;

    @Override
    public void zadzwon(String nrTel) {
        Random generator = new Random();
        int losowaLiczba = generator.nextInt(100);
        if (losowaLiczba < 40) {
            // udało się zadzwonić
            System.out.println("Zadzwoniłem na numer: " + nrTel);

            //* zadzwon() niech generuje losowy czas rozmowy w zakresie od
            //1 minuty do godziny
            int wylosowanyCzasRozmowy = generator.nextInt(60) + 1;
            lacznyCzasRozmow += wylosowanyCzasRozmowy;
            System.out.println("Rozmowa trwała: " + wylosowanyCzasRozmowy);
        } else if (losowaLiczba >= 40) {
            // nie udało się zadzwonić
            System.out.println("Nie udało się dodzwonić.");
        }
    }

    @Override
    public void zadzwonNaNrAlarmowy() {
        zadzwon(NR_ALARMOWY);
    }
}
