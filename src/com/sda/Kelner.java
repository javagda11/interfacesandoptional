package com.sda;

public class Kelner implements INasłuchujący, IPotrafiacyPisac{
    private String imie;

    public Kelner(String imie) {
        this.imie = imie;
    }

    @Override
    public void jakiesZdarzenie(String trescZdarzenia) {
        System.out.println("Ja " + imie + " zostaje powiadomiony o " + trescZdarzenia);
    }

    @Override
    public void napiszNaKartce(String tekst) {

    }
}
