package com.sda.zadX;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(new Random().nextInt(10));
        }

        System.out.println(list);
        int[] ileLiczb = new int[11];
        // ileLiczb[0] = ile jest wystapien zer
        // ileLiczb[1] = ile jest wystapien jedynek
        // ileLiczb[2] = ile jest wystapien dwojek
        // ileLiczb[3] = ile jest wystapien trojek
        // ileLiczb[4] = ile jest wystapien czworek
        // ileLiczb[5] = ile jest wystapien piatek
        // ...
        // ileLiczb[10] = ile jest wystapien dziesiątek

        for (int i = 0; i < list.size(); i++) {
//            ileLiczb[list.get(i)]++;

            if (list.get(i) == 0) {
                ileLiczb[0]++;
            } else if (list.get(i) == 1) {
                ileLiczb[1]++;
            }else if (list.get(i) == 2) {
                ileLiczb[2]++;
            }else if (list.get(i) == 3) {
                ileLiczb[3]++;
            }else if (list.get(i) == 4) {
                ileLiczb[4]++;
            }else if (list.get(i) == 5) {
                ileLiczb[5]++;
            }else if (list.get(i) == 6) {
                ileLiczb[6]++;
            }
            // ...
        }
        for (int i = 0; i < ileLiczb.length; i++) {
            System.out.println("Liczba " + i + " wystapila " + ileLiczb[i]);
        }
    }
}
