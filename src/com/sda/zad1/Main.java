package com.sda.zad1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Beben beben = new Beben();
//        Gitara gitara = new Gitara();
//        Pianino pianino = new Pianino();

//        Instrumentalny beben = new Beben();
//        Instrumentalny gitara = new Gitara();
//        Instrumentalny pianino = new Pianino();
//
        List<Instrumentalny> instrumenty = new ArrayList<>();

        Instrumentalny beben = new Beben();
        Instrumentalny gitara = new Gitara();
        Instrumentalny pianino = new Pianino();

        instrumenty.add(gitara);
        instrumenty.add(beben);
        instrumenty.add(pianino);

        for (Instrumentalny przyrzad : instrumenty) {
            przyrzad.graj();
        }
//
    }
}
