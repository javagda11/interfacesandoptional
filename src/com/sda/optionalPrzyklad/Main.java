package com.sda.optionalPrzyklad;

import com.sda.zad11.Corka;
import com.sda.zad11.ICzlonekRodziny;
import com.sda.zad11.Ojciec;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        List<ICzlonekRodziny> rodzina = new ArrayList<>();
//        rodzina.add(new Ojciec("Janusz"));
        rodzina.add(new Corka("Hela"));
        rodzina.add(new Corka("Helga"));

        Optional<ICzlonekRodziny> opcjonalnyTata = znajdzTate(rodzina);
        if(opcjonalnyTata.isPresent()) {
            ICzlonekRodziny tata = opcjonalnyTata.get();
            tata.przedstawSie();
        }else{
            System.out.println("Nie znalazlem taty.");
        }
    }

    public static Optional<ICzlonekRodziny> znajdzTate(List<ICzlonekRodziny> rodzina){
        for (ICzlonekRodziny czlonek : rodzina) {
            if(czlonek instanceof Ojciec){
                return Optional.ofNullable(czlonek);
            }
        }
        return Optional.empty();
    }
}
