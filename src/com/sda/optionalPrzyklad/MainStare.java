package com.sda.optionalPrzyklad;

import com.sda.zad11.Corka;
import com.sda.zad11.ICzlonekRodziny;
import com.sda.zad11.Ojciec;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MainStare {
    public static void main(String[] args) {
        List<ICzlonekRodziny> rodzina = new ArrayList<>();
//        rodzina.add(new Ojciec("Janusz"));
        rodzina.add(new Corka("Hela"));
        rodzina.add(new Corka("Helga"));

        ICzlonekRodziny tata = znajdzTate(rodzina);
        if(tata != null) {
            tata.przedstawSie();
        }else{
            System.out.println("Nie znalazlem taty.");
        }
    }

    public static ICzlonekRodziny znajdzTate(List<ICzlonekRodziny> rodzina){
        for (ICzlonekRodziny czlonek : rodzina) {
            if(czlonek instanceof Ojciec){
                return czlonek;
            }
        }
        return null;
    }
}
