package com.sda.optionalPrzyklad.zad1;

import java.util.List;
import java.util.Optional;

public class Ksiegarnia {
    private List<Ksiazka> zbiorKsiazek;

    public Ksiegarnia(List<Ksiazka> zbiorKsiazek) {
        this.zbiorKsiazek = zbiorKsiazek;
    }

    public Optional<Ksiazka> podajKsiazkeOTytule(String szukanyTytul){
        for (Ksiazka ksiazka : zbiorKsiazek) {
            System.out.println("Sprawdzam ksiazke: " + ksiazka);
            if(ksiazka.getTytul().equals(szukanyTytul)){
                return Optional.ofNullable(ksiazka);
            }
        }
        return Optional.empty(); // zamiast 'return null'
    }
}
