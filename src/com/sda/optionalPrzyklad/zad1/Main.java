package com.sda.optionalPrzyklad.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        List<Ksiazka> ksiazki = new ArrayList<>();
        ksiazki.add(new Ksiazka("W pustyni i w puszczy", 300, false));
        ksiazki.add(new Ksiazka("Iniemamocni", 50, false));
        ksiazki.add(new Ksiazka("Thinking in java", 350, false));
        ksiazki.add(new Ksiazka("Effective Java", 100, false));
        ksiazki.add(new Ksiazka("Krzyzacy", 200, false));
        ksiazki.add(new Ksiazka("Instrukcja obsługi rzutnika", 30, false));

        Ksiegarnia ksiegarnia = new Ksiegarnia(ksiazki);

        Optional<Ksiazka> znalezionaKsiazka = ksiegarnia.podajKsiazkeOTytule("Iniemamocni");
        if(znalezionaKsiazka.isPresent()){
            Ksiazka cos = znalezionaKsiazka.get();

        }else{

        }

    }
}
