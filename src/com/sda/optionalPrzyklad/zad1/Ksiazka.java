package com.sda.optionalPrzyklad.zad1;

public class Ksiazka {
    private String tytul;
    private int iloscStron;
    private boolean wypozyczona;

    public Ksiazka(String tytul, int iloscStron, boolean wypozyczona) {
        this.tytul = tytul;
        this.iloscStron = iloscStron;
        this.wypozyczona = wypozyczona;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public int getIloscStron() {
        return iloscStron;
    }

    public void setIloscStron(int iloscStron) {
        this.iloscStron = iloscStron;
    }

    public boolean isWypozyczona() {
        return wypozyczona;
    }

    public void setWypozyczona(boolean wypozyczona) {
        this.wypozyczona = wypozyczona;
    }

    @Override
    public String toString() {
        return "Ksiazka{" +
                "tytul='" + tytul + '\'' +
                ", iloscStron=" + iloscStron +
                ", wypozyczona=" + wypozyczona +
                '}';
    }
}
