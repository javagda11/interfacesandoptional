package com.sda;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<INasłuchujący> nasłuchującies = new ArrayList<>();
        nasłuchującies.add(new Kelner("Marian"));
        nasłuchującies.add(new Kelner("Jurek"));
        nasłuchującies.add(new Kelner("Olek"));
        nasłuchującies.add(new Kucharz());

        IGotujacy potrafiacyPisac = new Kucharz();
        potrafiacyPisac.ugotujCos();


        for (INasłuchujący nasluchujacy : nasłuchującies) {
            nasluchujacy.jakiesZdarzenie("Przyszedł klient!");
        }

    }
}
