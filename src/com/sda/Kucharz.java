package com.sda;

public class Kucharz implements INasłuchujący, IPotrafiacyPisac, IGotujacy {

    @Override
    public void jakiesZdarzenie(String trescZdarzenia) {
        System.out.println("Jestem kucharzem i zostaje powiadomiony o:" + trescZdarzenia);
    }

    public void przedstawSie(){
        System.out.println("Jestem kucharz!");
    }

    @Override
    public void napiszNaKartce(String tekst) {

    }

    @Override
    public void ugotujCos() {

    }
}
