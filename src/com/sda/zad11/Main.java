package com.sda.zad11;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<ICzlonekRodziny> rodzina = new ArrayList<>();
        rodzina.add(new Ojciec("Janusz"));
        rodzina.add(new Corka("Hela"));
        rodzina.add(new Corka("Helga"));

        for (ICzlonekRodziny czlonek : rodzina) {
            czlonek.przedstawSie();
            System.out.println("Jestem dorosły: " + czlonek.jestDorosly());
        }

        Ojciec czlonek = new Ojciec("awd");
        czlonek.zabij();
    }
}
