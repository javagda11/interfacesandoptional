package com.sda.zad11;

public class Ojciec implements ICzlonekRodziny, IZolnierz{
    private String imie;

    public Ojciec(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Moje imie : " + imie);
        ICzlonekRodziny.super.przedstawSie();
    }

    @Override
    public boolean jestDorosly() {
        return true;
    }

    public void zabij(){
        IZolnierz.super.przedstawSie();
    }
}
