package com.sda.zad11;

public class Corka implements ICzlonekRodziny {
    private String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Jestem corka! Mam na imie "+ imie);
    }

    @Override
    public boolean jestDorosly() {
        return false;
    }
}
