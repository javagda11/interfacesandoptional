package com.sda.zad11;

public interface ICzlonekRodziny {
    default void przedstawSie(){
        System.out.println("Jestem czlonkiem rodziny");
    }

    boolean jestDorosly();
}
